package ru.tsc.tambovtsev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends JpaRepository<Task, String> {

    Task findByUserIdAndId(String userId, String id);

    long countByUserId(String userId);

    List<Task> findAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

    boolean existsByUserIdAndId(String userId, String id);

}
