package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.dto.IRepository;
import ru.tsc.tambovtsev.tm.api.service.dto.IService;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractService<M extends AbstractEntityDTO, R extends IRepository<M>> implements IService<M> {

    @NotNull
    public abstract IRepository<M> getRepository();

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<M> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        repository.saveAll(models);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        repository.deleteAll();
        repository.saveAll(models);
        return models;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final M model) {
        Optional.ofNullable(model).orElseThrow(NullPointerException::new);
        @NotNull final IRepository<M> repository = getRepository();
        repository.save(model);
    }

}
