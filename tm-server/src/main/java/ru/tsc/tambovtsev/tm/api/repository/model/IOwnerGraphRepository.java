package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.List;

@NoRepositoryBean
public interface IOwnerGraphRepository<M extends AbstractUserOwnedModel> extends IGraphRepository<M> {

    M findByUserIdAndId(@Nullable String userId, @Nullable String id);

    List<M> findAllByUserId(@Nullable String userId, Pageable pageable);

    void deleteByUserId(@Nullable String userId);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

}
