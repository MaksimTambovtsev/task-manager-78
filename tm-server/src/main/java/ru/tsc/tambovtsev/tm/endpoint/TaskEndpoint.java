package ru.tsc.tambovtsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectTaskService;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.*;
import ru.tsc.tambovtsev.tm.enumerated.SortTable;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.dto.model.SessionDTO;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.exception.field.NameEmptyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @NotNull
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskService.create(task);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final SortTable sortTable = request.getSortTableType();
        return new TaskListResponse(taskService.findAll(userId, sortTable));
    }

    @NotNull
    @WebMethod
    public TaskSizeResponse sizeTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskSizeRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final TaskSizeResponse response = new TaskSizeResponse();
        response.setSize(taskService.getSize(userId));
        return response;
    }

    @NotNull
    @WebMethod
    public TaskBindProjectResponse bindProjectTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindProjectRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.bindTaskToProject(userId, projectId, id);
        return new TaskBindProjectResponse();
    }

    @NotNull
    @WebMethod
    public TaskUnbindProjectResponse unbindProjectTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindProjectRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        projectTaskService.unbindTaskFromProject(userId, id);
        return new TaskUnbindProjectResponse();
    }

    @NotNull
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        taskService.removeById(userId, id);
        return new TaskRemoveByIdResponse(null);
    }

    @NotNull
    @WebMethod
    public TaskShowByIdResponse showTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.findById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @Nullable final TaskDTO task = taskService.findById(userId, id);
        if (task == null) return new TaskUpdateByIdResponse(null);
        task.setId(id);
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskService.create(task);
        return new TaskUpdateByIdResponse(task);
    }

}
