package ru.tsc.tambovtsev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.marker.UnitCategory;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.Collection;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskServiceTest {

    @NotNull
    @Autowired
    private ITaskService service;

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    private final Task task = new Task("Task1", "Description");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task.setUserId(UserUtil.getUserId());
        service.save(task);
    }

    @After
    public void clean() {
        service.clear(UserUtil.getUserId());
    }

    @Test
    public void createTest() {
        @NotNull final String userId = UserUtil.getUserId();
        service.create(userId);
        Assert.assertEquals(2, service.count(userId));
    }

    @Test
    public void saveTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @NotNull final Task taskNew = new Task("Task2", "Description2");
        taskNew.setUserId(userId);
        service.save(taskNew);
        Assert.assertEquals(2, service.count(userId));
        @NotNull final Task taskFind = service.findById(userId, taskNew.getId());
        Assert.assertEquals(taskNew.getId(), taskFind.getId());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @Nullable final Task taskFind = service.findById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskFind.getId(), task.getId());
    }

    @Test
    public void findAllTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @Nullable final Collection<Task> taskList = service.findAll(userId);
        Assert.assertNotNull(taskList);
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void clearTest() {
        @NotNull final String userId = UserUtil.getUserId();
        service.clear(userId);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String userId = UserUtil.getUserId();
        service.removeById(userId, task.getId());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @NotNull final Task taskFind = service.findById(userId, task.getId());
        service.remove(taskFind);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, service.count(UserUtil.getUserId()));
    }
    
}
